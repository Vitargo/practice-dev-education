# Practice

## Practice 1

1. Дискриминант   ax^2+bx+c
2. Даны 4 числа типа int. Сравнить их и вывести наименьшее на консоль.
3. Вывести на консоль количество максимальных чисел среди этих четырех.
4. Даны 5 чисел (тип int). Вывести вначале наименьшее, а затем наибольшее из данных чисел.
5. Даны имена 2х человек (тип String). Если имена равны, то вывести сообщение о том, что люди являются тезками.
6. Дано число месяца (тип int). Необходимо определить время года (зима, весна, лето, осень) и вывести на консоль.

---

## Practice 2
1. While i=0 … i=-10.
2. While i=-20 … i=20.
3. While i=30 … i=10.
4. While i=1 … i=100 кратное семи.
5. While i=1 … i=100 кратное 3 и 5.
6. Те же 5 задач только с циклом for.
7. Сумма и произведение цифр числа.
8. Определить, какая цифра числа больше
9. Таблица умножения на 3.
10. Числа Фибоначчи первые 11.
11. На входе массив целых чисел int[] randomArray и целое число int a, необходимо вывести на экран все возможные пары чисел из randomArray, которые соответствовали правилу randomArray[i] + randomArray[j] = a. Пример: int randomArray = {6, 7, 1, 9, 1, 1, 0, 2, 3}
a = 12 result: 6 + 6 = 12, 9 + 3 = 12.

---

## Practice 3
1. Создайте массив из всех чётных чисел от 2 до 100 и выведите элементы массива на экран
2. Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в строку.
3. Создайте массив из 15 случайных целых чисел из отрезка [0;9]. Выведите массив на экран.
4. Создайте массив из 8 случайных целых чисел из отрезка [1;10]. Выведите массив на экран в строку.
5. Создайте массив из 4 случайных целых чисел из отрезка [10;99], выведите его на экран.
6. Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
7. Создайте массив из 12 случайных целых чисел из отрезка [-15;15]. Определите какой элемент является в этом массиве максимальным вывести его.

## Practice 4
11. Расставить пробелы после всех знаков препинания (, . : ? !)
12. Все символы строки (пути к файлу) c / перевернуть \\
13. Указать путь к файлу и провалидировать его (Pattern, Matcher) (на недопустимые символы)
14. Проверить, что указанный путь является диском
15. Проверить что это путь к файлу т.е путь должен заканчиваться // или \
16. Достать имя файла из пути. 
17. Проверить имя файла на допустимые символы.
18.  Выбрать все содержимое пути от диска к файлу.
