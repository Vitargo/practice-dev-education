package com.github.path;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang. NullPointerException;

public class AllTaskPatMat {
    public static void main(String[] args) {
        String s = null;
//        System.out.println("Task 1");
//        String sentence1 = "Hello!!!How are you?I am ok.And you? Your mood:sad or funny???";
//        System.out.println("Edit sentence: " + addSpace(sentence1));
//        System.out.println("Edit sentence: " + addSpacePatMat(sentence1));

//        System.out.println("Task 2");
        String path1 = "C:/Users/a.kuz/Desktop/";
//        System.out.println("Edit sentence: " + replaceSlash(path1));
//
//        System.out.println("Task 3");
        String path2 = "C__Users_a.kuz_Desktop";
//        System.out.println(validChar(path1));
//
//        System.out.println("Task 4");
//        System.out.println(checkIsDisk(path1));

//        System.out.println("Task 5");
//        String path3 = "C://User//a//kuz//Desktop//";
//        System.out.println(checkIsPath(path3));

        System.out.println("Task 6");
        String path4 = "C://User//a//kuz//Desktop//vita.exe";
        System.out.println(checkIsPath(path4));
    }

    public static String addSpace(String sentence){

        if(sentence == null) {
            String e = "Error! No text to edit!";
                return e;
        }
        List<Character> sentInAr = new ArrayList<>();
        for(char ch : sentence.toCharArray()){
            sentInAr.add(ch);
        }
        int size = sentInAr.size();
        for(int i = 0; i < size-1; i++){
            if(sentInAr.get(i) == '.' || sentInAr.get(i) == ',' || sentInAr.get(i) == ':' || sentInAr.get(i) == '!' || sentInAr.get(i) == '?'){
                if(sentInAr.get(i+1) != ' '){
                    sentInAr.add( i+1 , ' ');
                    size++;
                }
            }
        }
        StringBuilder newSentence = new StringBuilder(sentInAr.size());
        for(Character ch : sentInAr){
            newSentence.append(ch);
        }
        return newSentence.toString();
    }

    public static String addSpacePatMat(String sentence){
        if(sentence == null) {
            String e = "No text.";
            return e;
        }
        String newSentence = sentence.replaceAll("(?<=[,.:!?])(?=\\S)", " ");
        return newSentence;
    }

    public static String replaceSlash(String path){
        if(path == null) {
            String e = "Error! No text to edit!";
            return e;
        }
        return path.replaceAll("/", "\\\\\\\\");
    }

    public static String validChar(String path){
        if(path == null) {
            String e = "Error! No text to check!";
            return e;
        }
        Pattern pattern = Pattern.compile("[<>\"|\\\\?/:*]");
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()){
            return "The path consists invalids chars.";
        } else {
            return "The path is valid.";
        }
    }

    public static String checkIsDisk(String path){
        if(path == null) {
            String e = "Error! No text to check!";
            return e;
        }
        Pattern pattern = Pattern.compile("(^[a-zA-Z]:/)");
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()){
            return "Valid path.";
        } else {
            return "Not valid path.";
        }
    }

    public static String checkIsPath(String path){
        if(path == null) {
            String e = "Error! No text to check!";
            return e;
        }
        Pattern pattern = Pattern.compile("[//\\\\]$");
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()){
            return "Valid path.";
        } else {
            return "Not valid path.";
        }
    }

    public static String nameFile(String path){
        if(path == null) {
            String e = "Error! No text to check!";
            return e;
        }
        Pattern pattern = Pattern.compile("([?:]\w+\..{1,5})$");
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "Not valid path.";
        }
    }

}
