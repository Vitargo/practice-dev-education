package com.github.path;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AllTaskPatMatTest {

    // Task 1 (without Pattern Matcher)

    @Test
    public void addSpaceSmall(){
        String exp = ", . : ! ?";
        String act = AllTaskPatMat.addSpace(",.:!?");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceNull(){
        String exp = "Error! No text to edit!";
        String act = AllTaskPatMat.addSpace(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceZero(){
        String exp = "Hello";
        String act = AllTaskPatMat.addSpace("Hello");
        Assert.assertEquals(exp, act);
    }

    // Task 1 (with Pattern Matcher)

    @Test
    public void addSpaceSmall2(){
        String exp = ", . : ! ?";
        String act = AllTaskPatMat.addSpacePatMat(",.:!?");
        Assert.assertEquals(exp, act);
    }
    @Test
    public void addSpaceNull2(){
        String exp = "Error! No text to edit!";
        String act = AllTaskPatMat.addSpacePatMat(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceZero2(){
        String exp = "Hello";
        String act = AllTaskPatMat.addSpacePatMat("Hello");
        Assert.assertEquals(exp, act);
    }


    // Task 2
    @Test
    public void replaceSlashSmall(){
        String exp = "D:\\\\home\\\\desktop\\\\task2.java";
        String act = AllTaskPatMat.replaceSlash("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void replaceSlashNull(){
        String exp = "Error! No text to edit!";
        String act = AllTaskPatMat.replaceSlash(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void replaceSlashZero() {
        String exp = "Hello";
        String act = AllTaskPatMat.replaceSlash("Hello");
        Assert.assertEquals(exp, act);
    }

    // Task3

    @Test
    public void validCharBig(){
        String exp = "The path consists invalids chars.";
        String act = AllTaskPatMat.validChar("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void validCharNull(){
        String exp = "Error! No text to check!";
        String act = AllTaskPatMat.validChar(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void validCharZero() {
        String exp = "The path is valid.";
        String act = AllTaskPatMat.validChar("Hello");
        Assert.assertEquals(exp, act);
    }

    // Task 4

    @Test
    public void checkIsDiskBig(){
        String exp = "Valid path.";
        String act = AllTaskPatMat.checkIsDisk("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkIsDiskNull(){
        String exp = "Error! No text to check!";
        String act = AllTaskPatMat.checkIsDisk(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void checkIsDiskZero() {
        String exp = "Not valid path.";
        String act = AllTaskPatMat.checkIsDisk("Hello");
        Assert.assertEquals(exp, act);
    }

    // Task 5

    @Test
    public void checkIsPathBig(){
        String exp = "Valid path.";
        String act = AllTaskPatMat.checkIsPath("D:/home/desktop/task2.java/");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkIsPathNull(){
        String exp = "Error! No text to check!";
        String act = AllTaskPatMat.checkIsPath(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void checkIsPathZero() {
        String exp = "Not valid path.";
        String act = AllTaskPatMat.checkIsPath("C//Users\\a.kuz//Desktop");
        Assert.assertEquals(exp, act);
    }

}