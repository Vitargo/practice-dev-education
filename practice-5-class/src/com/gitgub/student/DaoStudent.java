package com.gitgub.student;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaoStudent {

    //Создать массив объектов. Вывести:

    //	a) список студентов заданного факультета;
    public static List<Student> facultyFinder(List<Student> students, String facultyFind){
        List<Student> studentOnFaculty = new ArrayList<>();
        for(Student s : students){
            if(s.getFaculty().equals(facultyFind)){
                studentOnFaculty.add(s);
            }
        }
        return studentOnFaculty;
    }

    //	b) списки студентов для каждого факультета и курса;
    public static HashMap<String, HashMap<String, ArrayList<Student>>> allStudentOnFaculty (List<Student> students){
        HashMap<String, HashMap<String, ArrayList<Student>>> course = new HashMap<>();
        for(Student s : students){
            if(!course.containsKey(s.getCourse())){
                HashMap<String, ArrayList<Student>> faculties = new HashMap<>();
                ArrayList<Student> studs = new ArrayList<>();
                studs.add(s);
                faculties.put(s.getFaculty(), studs);
                course.put(s.getCourse(), faculties);
            } else {
               HashMap<String, ArrayList<Student>> faculties = course.get(s.getCourse());
               ArrayList<Student> studs;

               if(!faculties.containsKey(s.getFaculty())){
                   studs = new ArrayList<>();
                   studs.add(s);
                   faculties.put(s.getFaculty(), studs);
               } else {
                   studs = faculties.get(s.getFaculty());
                   studs.add(s);
               }
            }
        }
        System.out.println(course.toString());
        return course;
    }

    //	c) список студентов, родившихся после заданного года;


    //	d) список учебной группы.


}
