package com.github.dialogs;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;

public class JCustomDialogs extends JFrame {

    public JCustomDialogs() throws HeadlessException {
        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton btnSimpMessage = new JButton("Simple message");
        JButton btnChooseColor = new JButton("Color");
        JButton btnFileChooser = new JButton("File Chooser");
        JButton btnConfMessage = new JButton("Confirm message");
        JButton btnQuestMessage = new JButton("Question message");

        btnSimpMessage.setBounds(10, 10, 120, 30);
        btnChooseColor.setBounds(10, 50, 120, 30);
        btnFileChooser.setBounds(10, 100, 120, 30);
        btnConfMessage.setBounds(10, 100, 120, 30);
        btnQuestMessage.setBounds(10, 100, 120, 30);

        btnSimpMessage.addActionListener(e -> OptionsDialogs.simpleMess());
        btnConfMessage.addActionListener(e -> OptionsDialogs.confirmMess());
        btnChooseColor.addActionListener(e -> {
            Color color = OptionsDialogs.colourMess();
            System.out.println(color);
        });
        btnQuestMessage.addActionListener(e -> {
            String s = OptionsDialogs.questionMess();
            System.out.println(s);
        });
        btnFileChooser.addActionListener(e -> {
            JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showOpenDialog(null);
        });

        add(btnFileChooser);
        add(btnChooseColor);
        add(btnSimpMessage);
        add(btnConfMessage);
        add(btnQuestMessage);

        setLayout(null);
        setVisible(Boolean.TRUE);
    }
}
