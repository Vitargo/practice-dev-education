package com.github.dialogs;

import javax.swing.*;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import java.awt.*;

public class OptionsDialogs {

    private static Object AbstractColorChooserPanel;

    public static void simpleMess() {
        JOptionPane.showMessageDialog(
                new JFrame(), "Hello World!", "Dialog", JOptionPane.ERROR_MESSAGE
        );
    }

    public static void confirmMess() {
        JOptionPane.showConfirmDialog(
                new JFrame(), "Do you want to eat?", "Dinner", JOptionPane.YES_NO_OPTION
        );
    }
    public static String questionMess() {
        return JOptionPane.showInputDialog(
                new JPanel(), "What do you want to eat?", "Dinner", JOptionPane.QUESTION_MESSAGE
        );
    }
    public static Color colourMess() {
        return JColorChooser.showDialog(
                new JFrame(), "Show the color", (Color) AbstractColorChooserPanel
        );
    }
}
