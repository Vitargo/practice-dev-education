package org.bitbucket.properties;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws IOException {
        File file1 = new File("/Users/victory/Documents/workspace/deveducation/practice-8-propertiess/src/main/resources/application-default.properties");
        Properties prop1 = new Properties();
        prop1.load(new FileReader(file1));
        Map<String, Object> properties1 = PropertiesLoader.toMapLoop(prop1);

        properties1.forEach((key, value) -> System.out.println(key + ":" + value));

        File file2 = new File("/Users/victory/Documents/workspace/deveducation/practice-8-propertiess/src/main/resources/application-dev.properties");
        Properties prop2 = new Properties();
        prop2.load(new FileReader(file2));
        Map<String, Object> properties2 = PropertiesLoader.toMapStream(prop2);

        properties2.forEach((key, value) -> System.out.println(key + ":" + value));
    }
}
