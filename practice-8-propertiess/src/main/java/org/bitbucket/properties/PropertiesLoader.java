package org.bitbucket.properties;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesLoader {

    public static HashMap<String, Object> toMapLoop(Properties prop) {
        HashMap<String, Object> retMap = new HashMap<>();
        for (Map.Entry<Object, Object> entry : prop.entrySet()) {
            retMap.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }
        return retMap;
    }

    public static HashMap<String, Object> toMapStream(Properties prop) {
        return prop.entrySet().stream().collect(
                Collectors.toMap(
                        e -> String.valueOf(e.getKey()),
                        e -> e.getValue(),
                        (prev, next) -> next, HashMap::new
                ));
    }

}
